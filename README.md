# DKX/NetteGCloudPubSub

Google cloud pub/sub integration for Nette DI

## Installation

```bash
$ composer require dkx/nette-gcloud
$ composer require dkx/nette-gcloud-pubsub
```

## Usage

```yaml
extensions:
    gcloud: DKX\NetteGCloud\DI\GCloudExtension
    gcloud.pubSub: DKX\NetteGCloudPubSub\DI\GCloudPubSubExtension
```

Now you'll be able to simply inject the `Google\Cloud\PubSub\PubSubClient`.
