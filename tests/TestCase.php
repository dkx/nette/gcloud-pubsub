<?php

declare(strict_types=1);

namespace DKXTests\NetteGCloudPubSub;

use Mockery;
use PHPUnit;

abstract class TestCase extends PHPUnit\Framework\TestCase
{
	public function tearDown() : void
	{
		parent::tearDown();
		self::addToAssertionCount(Mockery::getContainer()->mockery_getExpectationCount());
		Mockery::close();
	}
}
