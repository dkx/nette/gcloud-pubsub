<?php

declare(strict_types=1);

namespace DKXTests\NetteGCloudPubSub\Tests;

use DKX\NetteGCloud\Credentials\CredentialsProvider;
use DKX\NetteGCloud\ProjectId\ProjectIdProvider;
use DKX\NetteGCloudPubSub\PubSubClientFactoryImpl;
use DKXTests\NetteGCloudPubSub\TestCase;
use Mockery;
use function assert;

final class PubSubClientFactoryImplTest extends TestCase
{
	public function testCreate() : void
	{
		$projectIdProvider = Mockery::mock(ProjectIdProvider::class)
			->shouldReceive('getProjectId')->andReturn('abcd')->getMock();
		assert($projectIdProvider instanceof ProjectIdProvider);

		$credentialsProvider = Mockery::mock(CredentialsProvider::class)
			->shouldReceive('getCredentials')->andReturn([])->getMock();
		assert($credentialsProvider instanceof CredentialsProvider);

		$factory = new PubSubClientFactoryImpl($projectIdProvider, $credentialsProvider);
		$factory->create();
	}
}
