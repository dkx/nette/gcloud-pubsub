<?php

declare(strict_types=1);

namespace DKX\NetteGCloudPubSub;

use Google\Cloud\PubSub\PubSubClient;

interface PubSubClientFactory
{
	public function create() : PubSubClient;
}
