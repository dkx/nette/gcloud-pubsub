<?php

declare(strict_types=1);

namespace DKX\NetteGCloudPubSub;

use DKX\NetteGCloud\Credentials\CredentialsProvider;
use DKX\NetteGCloud\ProjectId\ProjectIdProvider;
use Google\Cloud\PubSub\PubSubClient;

final class PubSubClientFactoryImpl implements PubSubClientFactory
{
	private ProjectIdProvider $projectIdProvider;

	private CredentialsProvider $credentialsProvider;

	public function __construct(ProjectIdProvider $projectIdProvider, CredentialsProvider $credentialsProvider)
	{
		$this->projectIdProvider   = $projectIdProvider;
		$this->credentialsProvider = $credentialsProvider;
	}

	/**
	 * @codeCoverageIgnore
	 */
	public function create() : PubSubClient
	{
		return new PubSubClient([
			'projectId' => $this->projectIdProvider->getProjectId(),
			'keyFile' => $this->credentialsProvider->getCredentials(),
		]);
	}
}
