<?php

declare(strict_types=1);

namespace DKX\NetteGCloudPubSub\DI;

use DKX\NetteGCloudPubSub\PubSubClientFactory;
use DKX\NetteGCloudPubSub\PubSubClientFactoryImpl;
use Google\Cloud\PubSub\PubSubClient;
use Nette\DI\CompilerExtension;
use Nette\DI\Definitions\Statement;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use function assert;
use function is_object;
use function is_string;

/**
 * @codeCoverageIgnore
 */
final class GCloudPubSubExtension extends CompilerExtension
{
	public function getConfigSchema() : Schema
	{
		return Expect::structure([
			'factory' => Expect::anyOf(Expect::string(), Expect::type(Statement::class))
				->default(PubSubClientFactoryImpl::class),
		]);
	}

	public function loadConfiguration() : void
	{
		$builder = $this->getContainerBuilder();
		$config  = $this->getConfig();
		assert(is_object($config));

		$factory = $builder
			->addDefinition($this->prefix('factory'))
			->setType(PubSubClientFactory::class)
			->setAutowired(false);

		if (is_string($config->factory)) {
			$factory->setFactory($config->factory);
		} elseif ($config->factory instanceof Statement) {
			assert(is_string($config->factory->entity));
			$factory->setFactory($config->factory->entity, $config->factory->arguments);
		}

		$builder
			->addDefinition($this->prefix('client'))
			->setType(PubSubClient::class)
			->setFactory([$factory, 'create']);
	}
}
